defmodule Mset2Test do
  use ExUnit.Case

  @a [1, 1, 2, 3, 3, 3, 4, 4]
  @b [1, 2, 2, 3, 3, 3]

  test "new" do
    assert Mset2.new(@a) |> Enum.sort == [{1, 2}, {2, 1}, {3, 3}, {4, 2}]
    assert Mset2.new(@b) == [{1, 1}, {2, 2}, {3, 3}]
  end

  test "union" do
    a = Mset2.new(@a)
    b = Mset2.new(@b)
    assert Mset2.union(a, a) == a
    assert Mset2.union(a, b) == [{1, 2}, {2, 2}, {3, 3}, {4, 2}]
  end

  test "intersection" do
    a = Mset2.new(@a)
    b = Mset2.new(@b)
    assert Mset2.intersection(a, a) == a
    assert Mset2.intersection(a, b) == [{1, 1}, {2, 1}, {3, 3}]
  end
  
  test "inclusion" do
    a = Mset2.new(@a)
    b = Mset2.new(@b)
    assert Mset2.inclusion(a, a) == true
    assert Mset2.inclusion(a, b) == false
    assert Mset2.inclusion(Mset2.intersection(b,a), a) == true
  end
end
