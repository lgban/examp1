defmodule Mset1Test do
  use ExUnit.Case

  @a [1, 1, 2, 3, 3, 3, 4, 4]
  @b [1, 2, 2, 3, 3, 3]

  test "new" do
    assert Mset1.new(@a) == %{1 => 2, 2 => 1, 3 => 3, 4 => 2}
    assert Mset1.new(@b) == %{1 => 1, 2 => 2, 3 => 3}
  end

  test "union" do
    a = Mset1.new(@a)
    b = Mset1.new(@b)
    assert Mset1.union(a, a) == a
    assert Mset1.union(a, b) == %{1 => 2, 2 => 2, 3 => 3, 4 => 2}
  end

  test "intersection" do
    a = Mset1.new(@a)
    b = Mset1.new(@b)
    assert Mset1.intersection(a, a) == a
    assert Mset1.intersection(a, b) == %{1 => 1, 2 => 1, 3 => 3}
  end

  test "inclusion" do
    a = Mset1.new(@a)
    b = Mset1.new(@b)
    assert Mset1.inclusion(a, a) == true
    assert Mset1.inclusion(a, b) == false
    assert Mset1.inclusion(Mset1.intersection(b,a), a) == true
  end
end
